import os
import cgi
from html import escape
import cgitb; cgitb.enable()
import MySQLdb

"""
動画情報DBの中から検索して表示するプログラム
＜手順＞
# htmlの型の作成
# 検索条件を決定
# DBにアクセス
# 入力条件に従って検索
# 結果の整形
# 検索結果の表示
# DB処理終了時処理
"""

def set_emphasis(label):
    return "<b>{}</b>".format(label)

# htmlの型作成 #
html_upper = """
<html>
<head>
    <link rel="stylesheet" href="/css/show_search_result.css">
</head>
<body>\n
"""

html_lower = """
</body>
</html>
"""

content = ""

# 検索条件決定 #
message = cgi.FieldStorage()

# DBにアクセス #
with open("C:\\Users\\nflabs-54\\source\\repos\\secrets\\mysql_pass.txt", "r") as f:
    passwd = f.read()

conn = MySQLdb.connect(
 user='root',
 passwd=passwd,
 host='localhost',
 db='mysql',
 use_unicode=True,
 charset="utf8mb4")

# 検索 #
table_name = "video_info.video_info"
cur = conn.cursor()
sql = "SELECT * FROM {}".format(table_name)
title = ""
view_count_low = ""
view_count_up = ""
duration_low = ""
duration_up = ""
published_at_low = ""
published_at_up = ""
channel_title = ""

conditions = []
condition_values = []
if "title" in message:
    title = message["title"].value
    conditions.append("title COLLATE utf8mb4_unicode_ci LIKE %s".format(title))
    condition_values.append("%{}%".format(title))
if "view_count_low" in message:
    view_count_low = message["view_count_low"].value
    conditions.append("view_count >= %s")
    condition_values.append(view_count_low)
if "view_count_up" in message:
    view_count_up = message["view_count_up"].value
    conditions.append("view_count <= %s")
    condition_values.append(view_count_up)
if "duration_low" in message:
    duration_low = message["duration_low"].value
    conditions.append("duration >= %s")
    condition_values.append(duration_low)
if "duration_up" in message:
    duration_up = message["duration_up"].value
    conditions.append("duration <= %s")
    condition_values.append(duration_up)
if "published_at_low" in message:
    published_at_low = message["published_at_low"].value
    conditions.append("published_at >= %s")
    condition_values.append(published_at_low)
if "published_at_up" in message:
    published_at_up = message["published_at_up"].value
    conditions.append("published_at <= %s")
    condition_values.append(published_at_up)
if "channel_title" in message:
    channel_title = message["channel_title"].value
    conditions.append(r"channel_title LIKE %s")
    condition_values.append("%{}%".format(channel_title))

if conditions != []:
    sql += " WHERE "
    for i, condition in enumerate(conditions):
        if i != len(conditions) - 1:
            sql += "{} AND ".format(condition)
        else:
            sql += condition

cur.execute(sql, condition_values)


# 結果の整形(検索対象の強調、エスケープ処理) #
rows = cur.fetchall()
id_index = 0
title_index = 1
view_count_index = 2
published_at_index = 3
duration_index = 4
channel_title_index = 5
url_index = 6
image_path_index = 7

rows = list(rows)
for i, row in enumerate(rows):
    rows[i] = list(row)
    rows[i][url_index] = set_emphasis(escape(row[url_index]))
    if "title" in message:
        rows[i][title_index] = set_emphasis(escape(row[title_index]))
    if "view_count_low" in message or "view_count_up" in message:
        rows[i][view_count_index] = set_emphasis(escape(str(row[view_count_index])))
    if "duration_low" in message or "duration_up" in message:
        rows[i][duration_index] = set_emphasis(escape(str(row[duration_index])))
    if "published_at_low" in message or "published_at_up" in message:
        rows[i][published_at_index] = set_emphasis(escape(str(row[published_at_index])))
    if "channel_title" in message:
        rows[i][channel_title_index] = set_emphasis(escape(row[channel_title_index]))


# 結果を表示 #
content += """
<div class="search_result">
    <h1>検索結果</h1>
    <p>検索条件:</p>
    <div class="search_conditions">
        <p>動画タイトル: {}</p>
        <p>再生回数: {}回 ～ {}回</p>
        <p>再生時間: {} ～ {}</p>
        <p>アップロード日時: {} ～ {}</p>
        <p>チャンネル名: {}</p>
    </div>
    <hr>
</div>
""".format(
    escape(title), 
    escape(view_count_low), 
    escape(view_count_up), 
    escape(duration_low), 
    escape(duration_up), 
    escape(published_at_low), 
    escape(published_at_up), 
    escape(channel_title))


for row in rows:
    image_path = "/images/{}".format(os.path.basename(row[image_path_index]))
    content += """
        <div class="video_info">
            <a href="{}"><img src="{}" width=320px></a>
            <div>
                <p>タイトル: <a href="{}">{}</a></p>
                <p>再生回数: {}回</p>
                <p>アップロード日時: {}</p>
                <p>再生時間: {}</p>
                <p>チャンネル名: {}</p>
            </div>
        </div>
        """.format(
            row[url_index], 
            image_path, 
            row[url_index], 
            row[title_index], 
            row[view_count_index], 
            row[published_at_index], 
            row[duration_index], 
            row[channel_title_index])
    

# 終了時処理 #
cur.close()
conn.close()

html_text = html_upper + content + html_lower
print(html_text)