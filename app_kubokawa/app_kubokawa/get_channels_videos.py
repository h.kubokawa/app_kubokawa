import os
import json
import datetime
import requests
import MySQLdb
from apiclient.discovery import build
from oauth2client.tools import argparser

"""
登録チャンネル内の動画情報を取得するプログラム
＜手順＞
# APIキーの格納
# Youtube API認証
# 登録チャンネル情報を取得
# 各登録チャンネル内の動画情報を取得
# 各動画IDから詳細情報を取得
# サムネイルの保存
# DBにアクセス
# DBに動画情報を格納
"""

# APIキーの格納 #
with open("C:\\Users\\nflabs-54\\source\\repos\\secrets\\api_key.txt", "r") as f:
    API_KEY = f.read()


# Youtube API認証 #
API_SERVICE_NAME = "youtube"
API_VERSION = "v3"
youtube = build(API_SERVICE_NAME, API_VERSION, developerKey=API_KEY)


# 登録チャンネル情報の取得 #
options = {"part": "id", "channel_id": [], "max_results": 50}
options["channel_id"].append("UCSEoj94-efoQhREyRWRySKA")
options["channel_id"].append("UC3vg17IZ1IV73xx069jG44w")
options["channel_id"].append("UCRIgIJQWuBJ0Cv_VlU3USNA")
video_list = []


for channel_id in options["channel_id"]:
    # 各チャンネルの動画情報の取得(1回目) #
    channel_videos_info = youtube.search().list(
        part=options["part"],
        channelId=channel_id,
        maxResults=options["max_results"]
        ).execute()

    while True:
        # チャンネル内動画詳細情報の取得 #
        info_kinds = ["id", "snippet", "contentDetails", "statistics"]
        video_ids = []
        max_results = 50
        for channel_video_info in channel_videos_info["items"]:
            if "videoId" in channel_video_info["id"]:
                video_ids.append(channel_video_info["id"]["videoId"])

        if video_ids == []:
            break

        videos_info = youtube.videos().list(
            part=info_kinds,
            id=video_ids,
            maxResults=max_results).execute()["items"]

        JST = +9
        for video_info in videos_info:
            duration = video_info["contentDetails"]["duration"]
            if "S" in duration and not "M" in duration and not "H" in duration:
                iso_time_format = "PT%SS"
            elif not "S" in duration and "M" in duration and not "H" in duration:
                iso_time_format = "PT%MM"
            elif not "S" in duration and not "M" in duration and "H" in duration:
                iso_time_format = "PT%HH"

            elif "S" in duration and "M" in duration and not "H" in duration:
                iso_time_format = "PT%MM%SS"
            elif not "S" in duration and "M" in duration and "H" in duration:
                iso_time_format = "PT%HH%MM"
            elif "S" in duration and not "M" in duration and "H" in duration:
                iso_time_format = "PT%HH%SS"

            elif "S" in duration and "M" in duration and "H" in duration:
                iso_time_format = "PT%HH%MM%SS"

            utc_published_at = datetime.datetime.strptime(video_info["snippet"]["publishedAt"], "%Y-%m-%dT%H:%M:%SZ")
            jst_published_at = utc_published_at + datetime.timedelta(hours=JST)
            image_path = "{}\\\\images\\\\{}.jpg".format(os.getcwd().replace("\\", "\\\\"), video_info["id"])
            video_list.append({"id": video_info["id"],
                                "title": video_info["snippet"]["title"],
                                "view_count": video_info["statistics"]["viewCount"],
                                "published_at": jst_published_at.strftime("%Y-%m-%d %H:%M:%S"),
                                "duration": datetime.datetime.strptime(duration, iso_time_format).strftime("%H:%M:%S"),
                                "channel_title": video_info["snippet"]["channelTitle"],
                                "url": "https://www.youtube.com/watch?v={}".format(video_info["id"]),
                                "image_path": image_path})

            # サムネイルの保存 #
            response = requests.get(video_info["snippet"]["thumbnails"]["medium"]["url"])
            image = response.content
            with open(image_path, "wb") as f:
                f.write(image)

        if "nextPageToken" in channel_videos_info:
            options["nextPageToken"] = channel_videos_info["nextPageToken"]
            # 各チャンネルの動画情報の取得(2回目以降) #
            channel_videos_info = youtube.search().list(
                part=options["part"],
                channelId=channel_id,
                maxResults=options["max_results"],
                pageToken=options["nextPageToken"]
                ).execute()
        else:
            break

    

# DBにアクセス #
with open("C:\\Users\\nflabs-54\\source\\repos\\secrets\\mysql_pass.txt", "r") as f:
    passwd = f.read()

conn = MySQLdb.connect(
 user='root',
 passwd=passwd,
 host='localhost',
 db='mysql',
 use_unicode=True,
 charset="utf8mb4")

# 動画情報の格納 #
table_name = "video_info.video_info"
cur = conn.cursor()
sql = "TRUNCATE {};".format(table_name)
cur.execute(sql)
for video_info in video_list:
    sql = "INSERT INTO {} VALUES (\"{}\", \"{}\", {}, \"{}\", \"{}\", \"{}\", \"{}\", \"{}\");"\
        .format(table_name,
                video_info["id"],
                video_info["title"],
                video_info["view_count"],
                video_info["published_at"],
                video_info["duration"],
                video_info["channel_title"],
                video_info["url"],
                video_info["image_path"])
    try:
        cur.execute(sql)
    except MySQLdb._exceptions.IntegrityError as e:  # 重複時のエラーを回避
        pass

print("動画情報が格納されました。")

# 終了時処理 #
cur.close()
conn.commit()
conn.close()